---
title: "About"
date: 2018-06-12T20:56:06+05:00
draft: true
---

This website has been inspired by many of the insightful writings by Asadullah Ali,
Waitbutwhy, and James Clear (whose website's design has inspired this too).

